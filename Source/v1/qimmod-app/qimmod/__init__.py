#!/usr/bin/env python3


"""
QImMod - quick image modification tool.
""" """

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""


import argparse

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw


__description__ = "quick image modification tool"
__version__ = "1.0.0"


class ImageManipulator:
    """
    Image manipulator.
    """

    def __init__(self, image_path):
        self.__image = Image.open(image_path)

    def save(self, new_image_path):
        self.__image.save(new_image_path)

    def draw(self):
        return ImageDraw.Draw(self.__image)

    def add_text(self, text, text_font, text_size):
        draw = self.draw()
        font = ImageFont.truetype(font=text_font, size=text_size)

        draw.text((50, 50), text, fill=(255, 0, 0), font=font)


def main():
    """
    Main.
    """

    default_output_path = "./out.png"
    default_text_font = "/usr/share/fonts/roboto/Roboto-Regular.ttf"

    parser = argparse.ArgumentParser(
        description=f"%(prog)s - {__description__}",
        epilog="""Copyright (c) 2024, Maciej Barć <xgqt@xgqt.org>""",
    )
    parser.add_argument(
        "-V", "--version", action="version", version=f"%(prog)s {__version__}"
    )
    parser.add_argument(
        "--output_path",
        type=str,
        help="output path to save to",
        default=default_output_path,
    )

    parser.add_argument("--text", type=str, help="text to add to the image")
    parser.add_argument("--text_font", type=str, default=default_text_font)
    parser.add_argument("--text_size", type=int)

    parser.add_argument("input_path", type=str, help="input image path")

    args = parser.parse_args()
    image = ImageManipulator(image_path=args.input_path)

    if args.text:
        image.add_text(args.text, args.text_font, args.text_size)

    image.save(args.output_path)


if __name__ == "__main__":
    main()

# QImMod

Quick image manipulation using Python's PIL.

## License

### Code

Code in this project is licensed under the MPL, version 2.0.

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.
